export class Contact {
  userId: number;
  email: string;
  userName: string;
  mobileNumber: string;
  firstName: string;
  lastName: string;
  image: string;
}
