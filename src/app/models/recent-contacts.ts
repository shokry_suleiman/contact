export class RecentContact {
  contactId: string;
  created_ts: string;
  email: string;
  firstName: string;
  image: string;
  lastName: string;
  mobileNumber: string;
}
