import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RecentContactsService } from "../services/recent-contacts.service";
import { ContactsService } from "../services/contacts.service";
import { Contact } from "../models/contact";
import { RecentContact } from "../models/recent-contacts";
import { Router, ActivatedRoute } from "@angular/router";
import { trigger, transition, style, animate } from "@angular/animations";

declare var $: any;

@Component({
  selector: "app-contact-list",
  templateUrl: "./contact-list.component.html",
  styleUrls: ["./contact-list.component.scss"],
  animations: [
    trigger("inOutAnimation", [
      transition(":enter", [
        style({ opacity: "0" }),
        animate("1s ease-out", style({ opacity: "1" })),
      ]),
      transition(":leave", [
        style({ opacity: "1" }),
        animate("1s ease-out", style({ opacity: "0" })),
      ]),
    ]),
    trigger("fadeIn", [
      transition(":enter", [
        style({ opacity: "0" }),
        animate("1s ease-out", style({ opacity: "1" })),
      ]),
    ]),
  ],
})
export class ContactListComponent implements OnInit {
  recContacts: RecentContact[];
  allContacts: Contact[];
  allLetters: string[];
  originalContacts: Contact[];
  filterFlag: boolean = false;
  @ViewChild('mtf',{ static: false}) mtf: ElementRef;
  constructor(
    private recentContactsService: RecentContactsService,
    private contactsService: ContactsService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    // this.route.queryParams.subscribe(params => {})
  }

  ngOnInit() {
    this.init();
    this.getContacts();
    this.getRecentContacts();
  }

  init() {
    this.allLetters = [
      "A",
      "B",
      "C",
      "D",
      "E",
      "F",
      "G",
      "H",
      "I",
      "J",
      "K",
      "L",
      "M",
      "N",
      "O",
      "P",
      "Q",
      "R",
      "S",
      "T",
      "U",
      "V",
      "W",
      "X",
      "Y",
      "Z",
    ];
    // $(document).ready(function() {
    //   $(".scroll").click(function() {
    //     console.log("id==>", this.id)
    //     var target = document.getElementById('section'+this.id).offsetTop;
    //     document.getElementById('contact-list').scrollTop = target;
    //   });
    // });
  }

  getRecentContacts() {
    this.recentContactsService.getRecetContacts().subscribe(
      (data) => {
        console.log(`data=====>>>`, data);
        this.recContacts = data.data;
      },
      (error) => {
        console.log(`Error====>>>`, error);
      }
    );
  }

  getContacts() {
    this.contactsService.getContacts().subscribe(
      (data) => {
        this.contactsService.getAddedContacts().subscribe(
          (add) => {

            console.log(`data=====>>>`, data);
            console.log(`added=====>>>`, add);
            // let z = [...data.data];
            // add.forEach(element => {
            //   z.push(element)
            // });
            this.allContacts = [...add,...data.data];
            console.log(`before sort`, this.allContacts);
            this.allContacts = this.allContacts.sort((a, b) => {
              // console.log(`a`,a)
              // console.log(`b`,b)
              // console.log(`a.fir`,a.firstName);
              // console.log(`b.fir`, b.firstName)
              if (a.firstName == null || a.firstName == "") return 0;
              if (b.firstName == null || b.firstName == "") return 0;
              // a.firstName != null && b.firstName != null
              if (a.firstName.toLowerCase() < b.firstName.toLowerCase()) {
                // console.log("amer at fir");
                return -1;
              }
              if (a.firstName.toLowerCase() > b.firstName.toLowerCase()) {
                // console.log("amer at sec");
                return 1;
              }

              return 0;
            });
            //  console.log(`x`,x)
            //  console.log(`z`,z)
            this.allContacts.splice(
              this.allContacts.findIndex((item) => item.firstName === ""),
              1
            );

            console.log(`after sort`, this.allContacts);
            this.originalContacts = [...this.allContacts];

            // console.log(`all contacts`, this.allContacts)
          },
          (error) => {
            console.log(`Error====>>>`, error);
          }
        );
      },
      (error) => {
        console.log(`Error====>>>`, error);
      }
    );
  }

  searchContacts(event) {
    const val = event.target.value;
    if (val != "") {
      this.filterFlag = true;
      let contacts = [...this.originalContacts];
      // console.log(`contacts`,contacts)
      contacts = contacts.filter(
        (c) =>
          (c.firstName && c.firstName.indexOf(val)) !== -1 ||
          (c.lastName && c.lastName.indexOf(val)) !== -1
      );
      this.allContacts = [...contacts];
      console.log(`contacts===>>>`, contacts);
    } else {
      this.filterFlag = false;
      this.allContacts = [...this.originalContacts];
    }

    // console.log(`value ${val}`)
  }

  // checkLetter(firstName: any, index: number) {
  //   if(firstName != null) {
  //     let Initial = '';
  //     if (index === 0) {
  //       Initial = this.allContacts[0].firstName.charAt(0);
  //     } else {
  //       if ( this.allContacts[index - 1].firstName == null) {
  //         this.allContacts[index - 1].firstName = ''
  //       }
  //       if ( this.allContacts[index ].firstName == null) {
  //         this.allContacts[index].firstName = ''
  //       }
  //       if (this.allContacts[index].firstName.charAt(0).toLowerCase() === this.allContacts[index - 1].firstName.charAt(0).toLowerCase()) {
  //         Initial = '';
  //       }
  //       else {
  //         Initial = this.allContacts[index].firstName.charAt(0);
  //       }
  //     }
  //     return Initial.toUpperCase();
  //   }

  // }
  scrollTo(letter) {
    // console.log("leteer", letter);
    // console.log("am here");
    document
      .querySelector("#" + letter)
      .scrollIntoView({ behavior: "smooth", block: "center" });
  }

  navigateAddContact() {
    this.router.navigate(["/add-contact"]);
  }

  errorHandler(event) {
    // console.log("am herer ", event)
    // console.debug(event);
    event.target.src = "../../assets/images/default-image.png";
  }
  focusing() {
    console.log('focus')
    console.log("na", )
    // this.mtf.underlineRef.nativeElement.style.width = '100%'
    // 
  }
  bluring() {
    console.log('blur')
    // this.mtf.nativeElement.style.width = '100px'
  }
}
