import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { Contact } from "../models/contact";

@Injectable({
  providedIn: "root",
})
export class ContactsService {
  addedContacts: Contact[] = [];
  constructor(private http: HttpClient) {}

  setContact(contact: Contact) {
    this.addedContacts.push(contact);
  }
  getContacts(): Observable<any> {
    return this.http.get("../assets/contacts.json");
  }

  getAddedContacts(): Observable<Contact[]> {
    return of(this.addedContacts);
  }
}
