import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class RecentContactsService {
  constructor(private http: HttpClient) {}

  getRecetContacts(): Observable<any> {
    return this.http.get("../assets/recent-contact.json");
  }
}
