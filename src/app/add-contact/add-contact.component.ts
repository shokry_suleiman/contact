import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { NavigationExtras, Router } from "@angular/router";
import { Contact } from "../models/contact";
import { ContactsService } from "../services/contacts.service";
import { trigger, transition, style, animate } from "@angular/animations";

@Component({
  selector: "app-add-contact",
  templateUrl: "./add-contact.component.html",
  styleUrls: ["./add-contact.component.scss"],
  animations: [
    trigger("inOutAnimation", [
      transition(":enter", [
        style({ opacity: "0" }),
        animate("1s ease-out", style({ opacity: "1" })),
      ]),
      transition(":leave", [
        style({ opacity: "1" }),
        animate("1s ease-out", style({ opacity: "0" })),
      ]),
    ]),
    trigger("fadeIn", [
      transition(":enter", [
        style({ opacity: "0" }),
        animate("1s ease-out", style({ opacity: "1" })),
      ]),
    ]),
  ],
})
export class AddContactComponent implements OnInit {
  uploadedImage: any;
  contactForm: FormGroup = this.fb.group({
    image: ["", Validators.required],
    firstName: ["", Validators.required],
    lastName: ["", Validators.required],
    countryCode: [
      "",
      [Validators.required, Validators.pattern(/^(\+?\d{1,3}|\d{1,4})$/)],
    ],
    mobileNumber: [
      "",
      [Validators.required, Validators.pattern("[0-9]+")],
    ],
    email: ["", [Validators.required, Validators.email]],
  });
  submitted: boolean = false;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private contactsService: ContactsService
  ) {}

  ngOnInit() {}

  preview(files) {
    if (files.length === 0) {
      return;
    }

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      console.log("reader.result", reader.result);
      this.uploadedImage = reader.result;
    };
  }
  onFileChange(event) {
    if (event.target.files && event.target.files.length) {
      this.preview(event.target.files);
      const [file] = event.target.files;

      if (!file.type.startsWith("image")) {
        this.contactForm.get("image").setErrors({
          required: true,
        });
      } else {
        this.contactForm.patchValue({
          ["image"]: file,
        });
      }
    }
    // console.log(`contactForm`, this.contactForm.value);
  }

  public get fc() {
    return this.contactForm.controls;
  }

  save() {
    this.submitted = true;
    // console.log("subm",this.submitted)
    if (this.contactForm.invalid) {
      return;
    }

    /** for storing in memeory */
    const contact: Contact = {
      userId: null,
      email: this.contactForm.value.email,
      userName: null,
      firstName: this.contactForm.value.firstName,
      lastName: this.contactForm.value.lastName,
      mobileNumber:
        String(this.contactForm.value.countryCode) +
        String(this.contactForm.value.mobileNumber),
      image: this.uploadedImage,
    };

    /** for post request data  to server*/
    // const contact: Contact = {
    //   userId: null,
    //   email:this.contactForm.value.email,
    //   userName: null,
    //   firstName: this.contactForm.value.firstName,
    //   lastName: this.contactForm.value.lastName,
    //   mobileNumber:  String(this.contactForm.value.countryCode) + String(this.contactForm.value.mobileNumber),
    //   image:this.contactForm.value.image
    // }
    // console.log("contact", contact);

    // const navigationExtras: NavigationExtras = {
    //   state : {
    //     contact : this.contactForm.value
    //   }
    // }
    // this.router.navigate(['/contact-list'], navigationExtras)

    this.contactsService.setContact(contact);
    this.router.navigate(["/contact-list"]);
    // console.log(`contactForm`, this.contactForm.value)
  }
  cancel() {
    this.router.navigate(["/contact-list"]);
  }
}
