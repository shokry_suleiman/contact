import { Pipe, PipeTransform } from "@angular/core";
import { Contact } from "../models/contact";

@Pipe({
  name: "checkLetter",
})
export class CheckLetterPipe implements PipeTransform {
  transform(value: any, allContacts: Contact[], index): any {
    let firstName = value;
    if (firstName != null) {
      let Initial = "";
      if (index === 0) {
        Initial = allContacts[0].firstName.charAt(0);
      } else {
        if (allContacts[index - 1].firstName == null) {
          allContacts[index - 1].firstName = "";
        }
        if (allContacts[index].firstName == null) {
          allContacts[index].firstName = "";
        }
        if (
          allContacts[index].firstName.charAt(0).toLowerCase() ===
          allContacts[index - 1].firstName.charAt(0).toLowerCase()
        ) {
          Initial = "";
        } else {
          Initial = allContacts[index].firstName.charAt(0);
        }
      }
      return Initial.toUpperCase();
    }
  }
}
